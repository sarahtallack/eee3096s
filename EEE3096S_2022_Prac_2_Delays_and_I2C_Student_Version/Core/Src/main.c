/* USER CODE BEGIN Header */
/**
*************************************
Info:		STM32 I2C with DS3231 HAL
Author:		Amaan Vally
*************************************
In this practical you will learn to use I2C on the STM32 using the HAL. Here, we will
be interfacing with a DS3231 RTC. We also create functions to convert the data between Binary
Coded Decimal (BCD) and decimal.

Code is also provided to send data from the STM32 to other devices using UART protocol
by using HAL. You will need Putty or a Python script to read from the serial port on your PC.

UART Connections are as follows: red->5V black->GND white(TX)->PA2 green(RX;unused)->PA3.
Open device manager and go to Ports. Plug in the USB connector with the STM powered on. Check the port number (COMx).
Open up Putty and create a new Serial session on that COMx with baud rate of 9600.

https://www.youtube.com/watch?v=EEsI9MxndbU&list=PLfIJKC1ud8ghc4eFhI84z_3p3Ap2MCMV-&index=4

RTC Connections: (+)->5V (-)->GND D->PB7 (I2C1_SDA) C->PB6 (I2C1_SCL)
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef struct {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hour;
	uint8_t dayofweek;
	uint8_t dayofmonth;
	uint8_t month;
	uint8_t year;
} TIME;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//TASK 2
#define DELAY1 0	//Delay of 0 secs
#define DELAY2 1	//Delay of 60 secs

//TASK 4
//Define the RTC slave address
#define DS3231_ADDRESS 0xD0 //1101000 from datasheet (pg17) shifted to the left
#define FIRST_REG 0x00
#define REG_SIZE 0x01
#define EPOCH_2022 1640988000
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
char buffer[14];
uint8_t data [] = "Hello from STM32!\r\n";
uint8_t epoch_print[24];
char time_print[25];
TIME time;
char time_string[18];
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */
void HAL_UART_TxCpltCllback(UART_HandleTypeDef *huart);
void pause_sec(float x);

uint8_t decToBcd(int val);
int bcdToDec(uint8_t val);
void setTime (uint8_t sec, uint8_t min, uint8_t hour, uint8_t dow, uint8_t dom, uint8_t month, uint8_t year);
void getTime (void);
int epochFromTime(TIME time);
void timeToString(void);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void){

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();

  /* USER CODE BEGIN 2 */


  //TASK 6

  //Define the starting point
  int sec = 0;
  int min = 0;
  int hour = 12;
  int dow = 2;
  int dom = 20;
  int month = 9;
  int year = 22;

  //Set the starting time on the RTC
  setTime(decToBcd(sec),decToBcd(min),decToBcd(hour),decToBcd(dow),decToBcd(dom),decToBcd(month),decToBcd(year));

  //Transmit message from STM via UART, to show that code has started to run
  HAL_UART_Transmit(&huart2, data, sizeof(data), 1000);

  //Display bcdToDec and decToBcd functions
  uint8_t testBCD = 10;

  char bcd_print[9];
  //Store bcd time in bcd_print array
  sprintf(bcd_print, "BCD: %d\r\n", decToBcd(testBCD));

  //Transmit bcd value via UART
  HAL_UART_Transmit(&huart2, bcd_print, sizeof(bcd_print), 1000);

  uint8_t testDec = 16;

  char dec_print[15];
  //Store bcd time in bcd_print array
  sprintf(dec_print, "Decimal: %d\r\n\r\n", bcdToDec(testDec));

  //Transmit bcd value via UART
  HAL_UART_Transmit(&huart2, dec_print, sizeof(dec_print), 1000);


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8); // Toggle blue LED
	//TASK 1
	//First run this with nothing else in the loop and scope pin PC8 on an oscilloscope
	pause_sec(DELAY1);
	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);

	//TASK 6

	//sprintf(buffer, "%d \r\n", "");
	//This creates a string "55555555555555" with a pointer called buffer

	//Transmit data via UART
	//Blocking! fine for small buffers

	HAL_UART_Transmit(&huart2, buffer, sizeof(buffer), 1000);


	//get time from RTC
	getTime();
	timeToString();

	//Store output of timeToString in a time_print array
	sprintf(time_print, "Time: %s\r", time_string);

	//Transmit the current time via UART
	HAL_UART_Transmit(&huart2, time_print, sizeof(time_print), 1000);

	//Store epoch time in epoch_print array
	sprintf(epoch_print, "Epoch: %d\r\n\r\n",epochFromTime(time));

	//Transmit epoch time via UART
	HAL_UART_Transmit(&huart2, epoch_print, sizeof(epoch_print), 1000);

	//Delay of DELAY2 = 60 s
	pause_sec(DELAY2);

	//Toggle the LED
	HAL_GPIO_TogglePin(GPIOC, GPIO_PIN_8);

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel4_5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, LD4_Pin|LD3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void pause_sec(float x)
{
	/* Delay program execution for x seconds */

	//TASK 2
	//DELAY1 and DELAY2 have been defined

	//YOUR CODE HERE
	HAL_Delay(x*1000);
}

uint8_t decToBcd(int val)
{
    /* Convert normal decimal numbers to binary coded decimal*/

	//TASK 3
	uint8_t x1 = (val/10)<<4;	//get the 10s part of the val, shift to the left by 4 to get in form 0bxxxx0000
	uint8_t x2 = (val%10);		//get last digit of val
	return x1 + x2;				//return addition of x1 and x2
}

int bcdToDec(uint8_t val)
{
    /* Convert binary coded decimal to normal decimal numbers */

	//TASK 3
	int x1 = (val>>4)*10;	//shift val to right by 4 to get in form 0b0000xxxx, multiply by 10 to get 10s part of dec
	int x2 = (val&0x0F);	//logic & operator to remove first part of val
	return x1 + x2;			//return addition of x1 and x2
}

void setTime (uint8_t sec, uint8_t min, uint8_t hour, uint8_t dow, uint8_t dom, uint8_t month, uint8_t year)
{
    /* Write the time to the RTC using I2C */

	//TASK 4

	uint8_t set_time[7];


	//Store each input into the set_time array
	set_time[0] = sec;
	set_time[1] = min;
	set_time[2] = hour;
	set_time[3] = dow;
	set_time[4] = dom;
	set_time[5] = month;
	set_time[6] = year;

	//DS3231_ADDRESS, FIRST_REG, REG_SIZE have been defined
	HAL_I2C_Mem_Write(&hi2c1, DS3231_ADDRESS, FIRST_REG, REG_SIZE, set_time, 7, 1000);
}

void getTime (void)
{
    /* Get the time from the RTC using I2C */

	//TASK 4
	//Update the global TIME time structure

	uint8_t get_time[7];

	//DS3231_ADDRESS, FIRST_REG, REG_SIZE have been defined
	//Store time from RTC in get_time array
	HAL_I2C_Mem_Read(&hi2c1, DS3231_ADDRESS, FIRST_REG, REG_SIZE, get_time, 7, 1000);


	//Update global TIME time structure with each value stored in the get_time array, converting each value from BCD into decimal
	time.seconds = bcdToDec(get_time[0]);
	time.minutes = bcdToDec(get_time[1]);
	time.hour = bcdToDec(get_time[2]);
	time.dayofweek = bcdToDec(get_time[3]);
	time.dayofmonth = bcdToDec(get_time[4]);
	time.month = bcdToDec(get_time[5]);
	time.year = bcdToDec(get_time[6]);
}

int epochFromTime(TIME time){
    /* Convert time to UNIX epoch time */

	//define output seconds integer
	int seconds = 0;

	//add the number of seconds of the year since 2022
	seconds += (time.year-22)*31536000;//seconds in years since 2022 (60sec*60min*24hr*365day)

	//add the number of seconds that have passed for the month
	switch(time.month){
	case 2:
		seconds += 2678400; //seconds passed through to get to feb (60sec*60min*24hr*31day)
	break;
	case 3:
		seconds += 5097600; //seconds passed through to get to march (prev_sec+60sec*60min*24hr*28day)
	break;
	case 4:
		seconds += 7776000; //seconds passed through to get to april (prev_sec+60sec*60min*24hr*31day)
	break;
	case 5:
		seconds += 10368000; //seconds passed through to get to may (prev_sec+60sec*60min*24hr*30day)
	break;
	case 6:
		seconds += 13046400; //seconds passed through to get to june (prev_sec+60sec*60min*24hr*31day)
	break;
	case 7:
		seconds += 15638400; //seconds passed through to get to july (prev_sec+60sec*60min*24hr*30day)
	break;
	case 8:
		seconds += 18316800; //seconds passed through to get to august (prev_sec+60sec*60min*24hr*31day)
	break;
	case 9:
		seconds += 20995200; //seconds passed through to get to september (prev_sec+60sec*60min*24hr*31day)
	break;
	case 10:
		seconds += 23587200; //seconds passed through to get to october (prev_sec+60sec*60min*24hr*30day)
	break;
	case 11:
		seconds += 26265600; //seconds passed through to get to november (prev_sec+60sec*60min*24hr*31day)
	break;
	case 12:
		seconds += 28857600; //seconds passed through to get to december (prev_sec+60sec*60min*24hr*30day)
	break;
	default:
		seconds = seconds; //seconds in january is calculated from days
	}

	//add the number of seconds that have passed for the day in the month
	seconds += (time.dayofmonth-1)*86400; //seconds in one day (60sec*60min*24hr)

	//add the number of seconds that have passed for hours
	seconds += time.hour*3600; //seconds in one hour (60sec*60min)

	//add the number of seconds that have passed from the minutes
	seconds += time.minutes*60; //seconds in one minute (60sec)

	//add the final number of seconds
	seconds += time.seconds;

	return EPOCH_2022 + seconds;
}

void timeToString(void)
{
	/* Store the current time in a time_string array to be printed */
	time_string[0]=time.hour/10+'0';
	time_string[1]=time.hour%10+'0';
	time_string[2]=':';
	time_string[3]=time.minutes/10+'0';
	time_string[4]=time.minutes%10+'0';
	time_string[5]=':';
	time_string[6]=time.seconds/10+'0';
	time_string[7]=time.seconds%10+'0';
	time_string[8]=' ';
	time_string[9]=time.dayofmonth/10+'0';
	time_string[10]=time.dayofmonth%10+'0';
	time_string[11]='/';
	time_string[12]=time.month/10+'0';
	time_string[13]=time.month%10+'0';
	time_string[14]='/';
	time_string[15]=time.year/10+'0';
	time_string[16]=time.year%10+'0';
	time_string[17]='\n';
	sprintf(time_print, "Time: %s\r",time_string);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
